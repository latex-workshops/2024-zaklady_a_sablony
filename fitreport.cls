\DeclareOption{CZE}{\def\frlang{1}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{article}

\ifx\frlang\undefined
\else
\if\frlang 1
\usepackage{polyglossia}
\ClassWarning{class-name}{cestina}
\fi
\fi

% strukturování: definice, příklady, důkazy apod.
% dokumentace balíčku: https://ctan.org/pkg/amsthm
\usepackage{amsthm}
\theoremstyle{definition}
\newtheorem{definition}{Definice}
\theoremstyle{remark}
\newtheorem{example}{Příklad}


% definice vlastního příkazu
\newcommand{\reverse}{\mathrm{R}}

\usepackage{fancyhdr} % definice záhlaví a zápatí
\pagestyle{fancy} % přepnutí na to, co tímto balíčkem nastavíme
\fancyhf{} % co bude celkově v záhlaví i zápatí (vymazání defaultu)
\fancyhead[R]{Ahoj \thepage} % záhlaví v pravé části
\fancyfoot[L]{Muj dokument} % zápatí v levé části
\fancyfoot[R]{\leftmark} % název section
\fancyhead[L]{\rightmark} % název subsection

\setlength{\parindent}{0pt}

\usepackage{geometry}
\geometry{left=1cm,right=2cm}
