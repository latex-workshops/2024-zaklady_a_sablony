#!/bin/bash

if [ -z "$1" ]; then
	echo argument required: file name of TEX source >&2
	exit 1
fi

INFILE="$1"
FILE_BASE="${INFILE%.*}"
OUTFILE="$FILE_BASE".html

echo '<!DOCTYPE html><html lang="cs"><head><meta charset="utf-8"><title>Základy práce v LaTeXu | workshop FIT++</title></head><body><main><pre>' > "$OUTFILE"
cat "$INFILE" >> "$OUTFILE"
echo '</pre></main></body></html>' >> "$OUTFILE"
