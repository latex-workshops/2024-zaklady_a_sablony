Workshop LaTeX pro začátečníky 2024 (FIT++)
===================================================

Požadavky
---------

- základy práce v LaTeXu
- šablony
- 90 minut workshop + 30 minut dotazy

Scénář
------

Jde o workshop, snaha je, aby si všichni vše zkoušeli se mnou nebo i samostatně.

### Základy práce

0. Pracovat budeme v cloud editoru https://www.papeeria.com/ v Live Demo.
1. uděláme minimalistický hello world, postupně dva odstavce a nadpis
2. Zkompilujeme demo dokument (Papeeria Live demo) a prohlédneme náhled. Popíšeme části zdrojáku. Ukážeme možnosti kompilace a přepneme na `TeX Live 2019` a `XeLaTeX`.
3. Promítnu `vysledek-strukturovany_text.pdf`, studenti samostatně zkusí dojít k tomu stejnému.
6. Vložíme obsah souboru `ukazka-matematika.tex`, společně projdeme a vysvětlíme. Zdůrazním
    - odkaz na seznam symbolů v komentáři 
    - amsthm
    - vlastní sazbu reverze.
7. Promítnu `vysledek-matematika.pdf`, studenti pracují samostatně.

### Šablona

Tato část pouze jako ukázka (demo).

1. Založíme soubor fitreport.cls
2. Soubor sablona.tex založíme na třídě fitreport.
3. do `fitreport.cls` vložit `\LoadClass{article}` a zmínit přehled dalších tříd: základní, https://ctan.org/topic/class
4. přesun balíčků a vlastních příkazů do šablony
5. fancyhdr
6. odsazení odstavce
7. geometry a levý okraj
8. DeclareOption* a ProcessOptions
9. DeclareOption pro češtinu s def pro odložení kódu a ifx později
10. diskuse kdy balíček a kdy šablona


Vyhodnocení
-----------

- pozor na věci u samostatné práce, které nejsou ve zdrojáku ukázky (stalo se u strukturovaného textu)
